<?php

namespace App\Services;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersService
{
    /**
     * Creates a user
     *
     * @param UserRequest $request
     * @return mixed
     */
    public function create(UserRequest $request)
    {
        $data = $request->validated();

        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        if ($request->has('addToGroup')) $user->groups()->attach($request->get('addToGroup'));

        return $user;
    }
}