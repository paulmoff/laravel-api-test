<?php

namespace App\Services;

use App\Group;
use App\Http\Requests\GroupRequest;

class GroupsService
{
    /**
     * Creates a group
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return Group::create([
            'name' => $data['name'],
        ]);
    }

    /**
     * Updates group
     *
     * @param GroupRequest $request
     * @param Group $group
     */
    public function update(GroupRequest $request, Group $group)
    {
        $group->update($request->validated());

        if ($request->has('removeUser')) $group->users()->detach($request->get('removeUser'));
        if ($request->has('addUser')) $group->users()->attach($request->get('addUser'));
    }
}