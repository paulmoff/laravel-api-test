<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required|email|unique:users,email',
                    'password' => 'required|string|min:6',
                ];

            case 'PATCH':
                return [
                    'first_name' => 'sometimes|required',
                    'last_name' => 'sometimes|required',
                    'email' => 'sometimes|required|email|unique:users,email,' . $this->user->id,
                    'password' => 'sometimes|required|string|min:6',
                ];
        }
    }
}
