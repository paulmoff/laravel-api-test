<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Services\UsersService;
use App\User;

class UsersController extends Controller
{
    /**
     * @var UsersService
     */
    private $usersService;

    /**
     * UsersController constructor.
     * @param UsersService $usersService
     */
    public function __construct(UsersService $usersService)
    {
        $this->usersService = $usersService;
    }

    /**
     * Lists all users
     *
     * @return string
     */
    public function index()
    {
        return response()->json(User::all());
    }

    /**
     * Creates a user
     *
     * @param UserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UserRequest $request)
    {
        $user = $this->usersService->create($request);

        return response()->json($user, 201);
    }

    /**
     * Shows user info
     *
     * @param User $user
     * @return string
     */
    public function show(User $user)
    {
        // it's better to use Fractal for json

        if (request()->has('isActive')) {
            return response()->json(['active' => (boolean) $user->state], 200);
        }

        return response()->json($user, 200);
    }

    /**
     * Updates the user
     *
     * @param UserRequest $request
     * @param User $user
     */
    public function update(UserRequest $request, User $user)
    {
        $user->update($request->validated());
    }
}
