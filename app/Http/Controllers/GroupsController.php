<?php

namespace App\Http\Controllers;

use App\Group;
use App\Http\Requests\GroupRequest;
use App\Services\GroupsService;

class GroupsController extends Controller
{
    /**
     * @var GroupsService
     */
    private $groupsService;

    public function __construct(GroupsService $groupsService)
    {
        $this->groupsService = $groupsService;
    }

    /**
     * Lists all groups
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(Group::all());
    }

    /**
     * Creates a group
     *
     * @param GroupRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(GroupRequest $request)
    {
        $group = $this->groupsService->create($request->validated());

        return response()->json($group, 201);
    }

    /**
     * Updates the group
     *
     * @param GroupRequest $request
     * @param Group $group
     */
    public function update(GroupRequest $request, Group $group)
    {
        $this->groupsService->update($request, $group);
    }
}
