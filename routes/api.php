<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('users', 'UsersController@index')->name('users.index');
Route::post('users', 'UsersController@store')->name('users.store');
Route::get('users/{user}', 'UsersController@show')->name('users.show');
Route::patch('users/{user}', 'UsersController@update')->name('users.update');

Route::get('groups', 'GroupsController@index')->name('groups.index');
Route::post('groups', 'GroupsController@store')->name('groups.store');
Route::patch('groups/{group}', 'GroupsController@update')->name('groups.update');