<?php
namespace Tests\Feature;

use App\Group;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function it_shows_a_list_of_users()
    {
        $users = create(User::class, [], 5);

        $response = $this->getJson(route('users.index'))
            ->assertStatus(200);

        $this->assertCount(count($users), $response->json());
    }

    /** @test */
    public function a_user_can_be_created()
    {
        $userData = [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'password' => 'secret',
            'email' => 'john@doe.com',
        ];

        $this->postJson(route('users.store'), $userData)
            ->assertStatus(201);

        $this->assertDatabaseHas('users', ['email' => $userData['email']]);
    }

    /** @test */
    public function a_user_can_be_created_and_added_to_a_group()
    {
        $userData = [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'password' => 'secret',
            'email' => 'john@doe.com',
        ];

        $group = create(Group::class);

        $response = $this->postJson(route('users.store') . '?addToGroup=' . $group->id, $userData)
            ->assertStatus(201);

        $this->assertDatabaseHas('users', ['email' => $userData['email']]);
        $this->assertDatabaseHas('group_user', ['group_id' => $group->id, 'user_id' => $response->json('id')]);
    }

    /** @test */
    public function it_shows_information_about_a_user()
    {
        $user = create(User::class);

        $this->getJson(route('users.show', $user))
            ->assertStatus(200)
            ->assertJson($user->toArray());
    }

    /** @test */
    public function it_checks_that_user_exists_and_active()
    {
        $activeUser = create(User::class, ['state' => true]);
        $inactiveUser = create(User::class, ['state' => false]);

        // check active user
        $response = $this->getJson(route('users.show', $activeUser) . '?isActive')
            ->assertStatus(200);
        $this->assertEquals(true, $response->json('active'));

        // check inactive user
        $response = $this->getJson(route('users.show', $inactiveUser) . '?isActive')
            ->assertStatus(200);
        $this->assertEquals(false, $response->json('active'));

        // check non-existent user
        $this->getJson(route('users.show', 999) . '?isActive')
            ->assertStatus(404);
    }

    /** @test */
    public function a_user_can_be_updated()
    {
        $user = create(User::class);

        $userData = [
            'first_name' => 'Jane',
            'email' => 'Jane@doe.com',
        ];

        $this->patchJson(route('users.update', $user), $userData)
            ->assertStatus(200);

        $this->assertDatabaseHas('users', $userData);
    }

    /** @test */
    public function user_can_belong_to_many_groups()
    {
        $user = create(User::class);
        $groups = create(Group::class, [], 3);

        $this->assertInstanceOf(Collection::class, $user->groups);

        $user->groups()->attach($groups);

        $this->assertCount(count($groups), $user->fresh()->groups);
    }
}

