<?php
namespace Tests\Feature;

use App\Group;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GroupsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_shows_a_list_of_groups()
    {
        $groups = create(Group::class, [], 5);

        $response = $this->getJson(route('groups.index'))
            ->assertStatus(200);

        $this->assertCount(count($groups), $response->json());
    }

    /** @test */
    public function group_can_be_created()
    {
        $group = make(Group::class);

        $this->postJson(route('groups.store'), $group->toArray())
            ->assertStatus(201);

        $this->assertDatabaseHas('groups', $group->toArray());
    }

    /** @test */
    public function group_info_can_be_modified()
    {
        $group = create(Group::class);
        $data = ['name' => 'test'];

        $this->patchJson(route('groups.update', $group), $data)
            ->assertStatus(200);

        $this->assertDatabaseHas('groups', $data);
    }

    /** @test */
    public function a_user_can_be_removed_from_group()
    {
        $group = create(Group::class);
        $user = create(User::class);

        $group->users()->attach($user);

        $this->assertCount(1, $group->users);

        $this->patchJson(route('groups.update', $group) . '?removeUser=' . $user->id, [])
            ->assertStatus(200);

        $this->assertCount(0, $group->fresh()->users);
    }

    /** @test */
    public function a_user_can_be_added_to_group()
    {
        $group = create(Group::class);
        $user = create(User::class);

        $this->assertCount(0, $group->users);

        $this->patchJson(route('groups.update', $group) . '?addUser=' . $user->id, [])
            ->assertStatus(200);

        $this->assertCount(1, $group->fresh()->users);
    }

    /** @test */
    public function group_can_have_many_related_users()
    {
        $group = create(Group::class);
        $users = create(User::class, [], 3);

        $this->assertInstanceOf(Collection::class, $group->users);

        $group->users()->attach($users);

        $this->assertCount(count($users), $group->fresh()->users);
    }
}
