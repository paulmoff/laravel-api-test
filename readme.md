## Installation

Git clone this repository, cd into it and run `composer install`

Run command `./install` or `bash install` (Composer should be installed globally, otherwise you will have to run commands from install script one-by-one manually).

## Tests

There are phpunit Feature tests in tests folder

## API methods:
 - /users/ - fetch(retrieve) list of users
 - POST /users/ - create a user
    - /users?addToGroup=[group_id] - create a user and add to a group
 - GET /users/id/ - fetch info of a user
    - /users/id?isActive - returns 404, if user does not exist, otherwise returns boolean(state field)
 - /users/id/ - modify users info 
 - /groups/ - fetch list of groups
 - /groups/ - create a group
 - PATCH /groups/id/ - modify group info
    - /groups/id?removeUser=[user_id] - removes a user from the group
    - /groups/id?addUser=[user_id] - adds a user to the group
